package az.demo.interfaceSet;

import java.util.HashSet;
import java.util.Iterator;

public class ClassHashSet {

    // HashSet doesn’t maintain any insertion order of elements.
    // Does not store elements as in indexing approach
    // use hashCode for elements which added
    // Data is unordered due hashCode. order random 
    static HashSet<String> hashSet = new HashSet<>();

    public static void main(String[] args) {

        System.out.println("Adding String2 to hashSet");
        hashSet.add("String2");

        System.out.println("Adding String1 to hashSet");
        hashSet.add("String1");


        System.out.println("Adding String3 to hashSet");
        hashSet.add("String3");

        System.out.println("Adding String3 to hashSet that already exist");
        hashSet.add("String3");

        System.out.println("Adding String3 to hashSetwith new keyword");
        hashSet.add(new String("String3"));

        System.out.println(hashSet);

        System.out.println("Is contains 'String1' : " + hashSet.contains("String1"));
        System.out.println("Is contains 'String4' : " + hashSet.contains("String4"));

        System.out.println("Size of hashSet : " + hashSet.size());

        hashSet.remove("String1");
        System.out.println("After removing String1 :" + hashSet);

        // loop hashSet
        Iterator<String> iterator = hashSet.iterator();
        while (iterator.hasNext()){
            System.out.println(iterator.next());
        }
    }
}
